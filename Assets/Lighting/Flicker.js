#pragma strict

var minIntensity : float;
var maxIntensity : float;

var maxAngle: float;
var minAngle: float;

var flickerFactor : float = 0.2;

private var modifier = -1;

function FixedUpdate()
{
	gameObject.light.intensity += modifier * flickerFactor * Time.deltaTime;
	gameObject.light.spotAngle += modifier * flickerFactor * Time.deltaTime;
		
	if(gameObject.light.intensity >= maxIntensity || gameObject.light.intensity <= minIntensity)
		modifier *= -1;
}

function Reset()
{
	maxIntensity = gameObject.light.intensity;
	minIntensity = gameObject.light.intensity - 0.6;
	
	maxAngle = gameObject.light.spotAngle + 5;
	maxAngle = gameObject.light.spotAngle - 5;
}