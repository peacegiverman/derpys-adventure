#pragma strict

var lifeText : GUIText;
var muffinText : GUIText;

private var derpyInfo : DerpyStatus;

function Awake()
{
	derpyInfo = FindObjectOfType(DerpyStatus);
}

function Update() {
	// ""+ because UnityScript won't cast to string
	if(lifeText) lifeText.text = ""+derpyInfo.lives;
	if(muffinText) muffinText.text = ""+derpyInfo.muffins;
}