#pragma strict
var breakEffect : ParticleSystem;

var hitSound : AudioClip;

var muffinPrefab : GameObject;

function Break()
{
	breakEffect.transform.position = transform.position;
	breakEffect.Play();
	
	if(muffinPrefab)
		Instantiate(muffinPrefab, transform.position, Quaternion.identity);
		
	Destroy(gameObject);
}

function Start () {
}

function Awake() {
}