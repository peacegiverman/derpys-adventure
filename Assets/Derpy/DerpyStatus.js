#pragma strict

var lives : int = 5;
var muffins : int = 0;
var invulnerableTime : float = 1.2;
var flickerRate : float = 0.1;

private var hitTimer : float = 0;
private var flickerTimer : float = 0;

private var renderers : Renderer[];
private var shaderType : String = "Diffuse";

private var characterController : CharacterController;

function Start()
{
	renderers = gameObject.GetComponentsInChildren.<Renderer>();
	characterController = GetComponent(CharacterController);
}

function FixedUpdate() {
	Flicker();
	hitTimer -= Time.fixedDeltaTime;
	
}

function AddMuffin() {
	muffins += 1;
}

function Hit()
{
	characterController.detectCollisions = false;

	if(hitTimer <= 0)
	{
		hitTimer = invulnerableTime;
	
		if(lives > 1)
		{
			lives--;
			var renderers : Renderer[];
			
		}
		else
			//GameOver();
			Debug.Log("GameOver");
	}
}

function Flicker()
{
	var newColor : Color;
	
	if(hitTimer > 0 && Time.fixedTime > flickerTimer)
	{
		flickerTimer = Time.fixedTime + flickerRate;
		
		if(shaderType === "Diffuse")
			shaderType = "Transparent/Diffuse";
		else
			shaderType = "Diffuse";
			
		for(var r : Renderer in renderers)
		{
				r.material.shader = Shader.Find(shaderType);
				newColor = r.material.color;
				newColor.a = (invulnerableTime - hitTimer) / invulnerableTime / 2;
				r.material.color = newColor;
		}
	}
	
	else if(hitTimer <= 0)
	{
		if(shaderType === "Transparent/Diffuse")
		{
			shaderType = "Diffuse";
			
			for(var r : Renderer in renderers)
			{
				r.material.shader = Shader.Find(shaderType);
				newColor = r.material.color;
				newColor.a = 1;
				r.material.color = newColor;
			}
		}
		
		if(!characterController.detectCollisions)
			characterController.detectCollisions = true;
			
		rigidbody.WakeUp();
	}
}