#pragma strict

private var derpyStatus : DerpyStatus;

function Awake()
{
	Debug.Log("Awake");
	derpyStatus = GetComponent(DerpyStatus);
}

function Update()
{
	rigidbody.WakeUp();
}

function IsAbove(y : double)
{
	return transform.position.y > y;
}
function OnCollisionEnter(collision : Collision)
{
	Debug.Log("CollisionDerp");
	derpyStatus.Hit();
}

function OnControllerColliderHit(collision : ControllerColliderHit)
{
	var controller : ThirdPersonController = GetComponent(ThirdPersonController); 

	if(collision.gameObject.CompareTag("Crate"))
	{
		if(controller.IsJumping() && IsAbove(collision.transform.position.y))
			collision.gameObject.SendMessage("Break");
	}
	else if(collision.gameObject.CompareTag("Enemy"))
	{
		//Debug.Log(controller.IsJumping());
		if(controller.IsJumping() && IsAbove(collision.transform.position.y))
		{
			Debug.Log("Derp: "+transform.position.y);
			Debug.Log("Bug: "+collision.transform.position.y);
			collision.gameObject.SendMessage("Die");
		}
		else
		{
			Debug.Log("CCH-Derpy");
			derpyStatus.Hit();
		}
	}
}

function OnTriggerEnter(other : Collider)
{
/* 	if(other.gameObject.CompareTag("Enemy"))
		derpyStatus.Hit();
	else */
	Debug.Log("TriggerEnter");
		other.gameObject.SendMessage("TriggerAction", derpyStatus);
}