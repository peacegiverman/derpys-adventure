#pragma strict

var onEnterCam : Camera;
var onExitCam : Camera;

function OnTriggerEnter(collider : Collider)
{
	Debug.Log("Trigger in cam");
	Debug.Log("Current: " + Camera.current);

	if(onEnterCam && collider.CompareTag("Player"))
	{	
		Camera.main.depth = -1;
		onEnterCam.depth = 1;
	}
}

function OnTriggerExit(collider : Collider)
{
		
	if(onExitCam && collider.CompareTag("Player"))
	{
		Camera.main.depth = -1;
		onExitCam.depth = 1;
	}
}