#pragma strict

var cam : Camera;

private var moving : boolean = false;
private var player : DerpyStatus;

function MoveAndScale()
{
	//move towards (upper-right) position on screen
	var viewPos : Vector3 = cam.WorldToViewportPoint(transform.position);
	//check if viewPos in position; delete if it is
	/* on delete increase muffin count:
		player.sendMessage("AddMuffin");
	*/
	
	//TODO: promenljiva pozicija odletanja umesto fiksne (0.8, 0.8)
	if(Vector3.Distance(viewPos, Vector3(0.8, 0.8)) < 0.5)
	{
		player.AddMuffin();
		Destroy(gameObject);
	}
	
	viewPos = Vector3.MoveTowards(viewPos, Vector3(0.8, 0.8), 0.2);
	transform.position = cam.ViewportToWorldPoint(viewPos);
	
	//scale down
	transform.localScale *= 0.90;
}

function Update()
{
	if(moving)
		MoveAndScale();
}

function TriggerAction(derpy : DerpyStatus)
{	
	moving = true;
	player = derpy;
}

function Awake()
{
	cam = FindObjectOfType(Camera);
}