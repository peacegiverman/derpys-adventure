#pragma strict

var dieEffect : ParticleSystem;
var velocity = 1.0;

var rotationOffset = Vector3(0,0,0);

var startPos : Vector3; 
var endPos : Vector3;

private var dir : Vector3;

private var bAngle1 : float;
private var bAngle2 : float; 
private var eAngle1 : float;
private var eAngle2 : float;

function Reset()
{
	startPos = Vector3(-4, transform.position.y, transform.position.z);
	endPos = Vector3(4, transform.position.y, transform.position.z);
}
function Start () {
	
	transform.LookAt(endPos);
	transform.Rotate(rotationOffset);
	//beginning angle range
	bAngle1 = (transform.eulerAngles.y - 0.1) % 360;
	bAngle2 = (transform.eulerAngles.y + 0.1) % 360;
	
	//end angle range (179.9 - 180.1 rotation ~ around 180)
	eAngle1 = (transform.eulerAngles.y + 179.9) % 360;
	eAngle2 = (transform.eulerAngles.y + 180.1) % 360;
	
	dir = (endPos - startPos).normalized;
}

function FixedUpdate ()
{
	//Debug.Log("AngleY: "+eAngle1);

	if( ((transform.eulerAngles.y >= bAngle1 && transform.eulerAngles.y <= bAngle2) 
			&& (Vector3.Distance(transform.position, endPos) <= 0.1)) ||
		((transform.eulerAngles.y >= eAngle1 && transform.eulerAngles.y <= eAngle2) 
			&& (Vector3.Distance(transform.position, startPos) <= 0.1) ))
	{
			dir = -dir;
			
			transform.Rotate(Vector3.up * 180);
	}
	
	//transform.position += velocity * dir * Time.deltaTime;
	rigidbody.velocity = velocity * dir;
}

function Die()
{
	dieEffect.transform.position = transform.position;
	dieEffect.Play();
	Destroy(gameObject);
}