#pragma strict

function OnTriggerEnter(c : Collider)
{
	Debug.Log("Trigger");
}

function OnControllerColliderHit(collider : ControllerColliderHit)
{
	Debug.Log("CCH");
}

function OnCollisionEnter(collision : Collision)
{
	Debug.Log("Collision");
	collision.gameObject.SendMessage("Hit");
}